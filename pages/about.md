---
layout: page
title: About
description: 不积小流无以成江海 不积跬步无以至千里
keywords: CheneyYi Han, 恒成立
comments: true
menu: 关于
permalink: /about/
---

我是恒成立，每天积累一点。

仰慕「优雅编码的艺术」。

坚信熟能生巧，努力改变人生。

## 联系

{% for website in site.data.social %}
* {{ website.sitename }}：[@{{ website.name }}]({{ website.url }})
{% endfor %}

## Skill Keywords

{% for category in site.data.skills %}
### {{ category.name }}
<div class="btn-inline">
{% for keyword in category.keywords %}
<button class="btn btn-outline" type="button">{{ keyword }}</button>
{% endfor %}
</div>
{% endfor %}
